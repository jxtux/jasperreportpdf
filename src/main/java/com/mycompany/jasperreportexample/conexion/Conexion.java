package com.mycompany.jasperreportexample.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    public static Connection conectar() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection con = null;

        try {
            String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
            Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            con = DriverManager.getConnection(url,"HR","oracle");
            if (con != null) {
                System.out.println("Conexion Satisfactoria");
            }

        } catch (SQLException e) {
            System.out.println("error:"+e.getMessage());
        }
        return con;
    }
}
