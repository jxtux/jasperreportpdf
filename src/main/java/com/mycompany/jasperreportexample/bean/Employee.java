package com.mycompany.jasperreportexample.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal EMPLOYEE_ID;
    private String FIRST_NAME;
    private String LAST_NAME;
    private String EMAIL;
    private String PHONE_NUMBER;
    private Date HIRE_DATE;
    private String JOB_ID;
    private BigDecimal SALARY;
    private BigDecimal COMMISSION_PCT;
    private BigDecimal MANAGER_ID;
    private BigDecimal DEPARTMENT_ID;

    public Employee(BigDecimal EMPLOYEE_ID, String FIRST_NAME, String LAST_NAME, BigDecimal SALARY) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
        this.FIRST_NAME = FIRST_NAME;
        this.LAST_NAME = LAST_NAME;
        this.SALARY = SALARY;
    }

    public BigDecimal getEMPLOYEE_ID() {
        return EMPLOYEE_ID;
    }

    public void setEMPLOYEE_ID(BigDecimal EMPLOYEE_ID) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public BigDecimal getSALARY() {
        return SALARY;
    }

    public void setSALARY(BigDecimal SALARY) {
        this.SALARY = SALARY;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public void setPHONE_NUMBER(String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER;
    }

    public Date getHIRE_DATE() {
        return HIRE_DATE;
    }

    public void setHIRE_DATE(Date HIRE_DATE) {
        this.HIRE_DATE = HIRE_DATE;
    }

    public String getJOB_ID() {
        return JOB_ID;
    }

    public void setJOB_ID(String JOB_ID) {
        this.JOB_ID = JOB_ID;
    }

    public BigDecimal getCOMMISSION_PCT() {
        return COMMISSION_PCT;
    }

    public void setCOMMISSION_PCT(BigDecimal COMMISSION_PCT) {
        this.COMMISSION_PCT = COMMISSION_PCT;
    }

    public BigDecimal getMANAGER_ID() {
        return MANAGER_ID;
    }

    public void setMANAGER_ID(BigDecimal MANAGER_ID) {
        this.MANAGER_ID = MANAGER_ID;
    }

    public BigDecimal getDEPARTMENT_ID() {
        return DEPARTMENT_ID;
    }

    public void setDEPARTMENT_ID(BigDecimal DEPARTMENT_ID) {
        this.DEPARTMENT_ID = DEPARTMENT_ID;
    }
}
