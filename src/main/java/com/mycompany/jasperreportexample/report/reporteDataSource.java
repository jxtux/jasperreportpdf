package com.mycompany.jasperreportexample.report;

import com.mycompany.jasperreportexample.bean.Employee;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class reporteDataSource {

    public static void main(String[] args) throws JRException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        //PARAMETROS
        Map parametros = new HashMap();
        parametros.put("departamentId", 100);

        List<Employee> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Employee e = new Employee(new BigDecimal(i), "name " + i, "apelllido " + i, new BigDecimal(5000));
            list.add(e);
        }

        File f = new File("C:\\Java\\Workspace\\ProjectOracleJasper\\listEmployeeDataSource.jasper");
        JasperReport reporte = (JasperReport) JRLoader.loadObject(f);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(list));

        JRPdfExporter exporter = new JRPdfExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("reportEmployee.pdf"));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        exporter.setConfiguration(configuration);

        exporter.exportReport();

//        JRExporter exporter = new JRPdfExporter();
//        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//        exporter.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File("reporteDataSource.pdf"));
//        exporter.exportReport();
        // se muestra en una ventana aparte para su descarga
        JasperPrint jasperPrintWindow = JasperFillManager.fillReport("C:\\Java\\Workspace\\ProjectOracleJasper\\listEmployeeDataSource.jasper", parametros, new JRBeanCollectionDataSource(list));
        JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow);
        jasperViewer.setVisible(true);
    }
}
