package com.mycompany.jasperreportexample.report;

import com.mycompany.jasperreportexample.conexion.Conexion;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class reporteConexion {

    public static void main(String[] args) throws JRException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        //PARAMETROS
        Map parametros = new HashMap();
        parametros.put("departamentId", 100);
        
        // descarga dentro del mismo proyecto
        JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Java\\Workspace\\ProjectOracleJasper\\listEmployeeConexion.jasper", parametros, Conexion.conectar());
        JRPdfExporter exp = new JRPdfExporter();
        exp.setExporterInput(new SimpleExporterInput(jasperPrint));
        exp.setExporterOutput(new SimpleOutputStreamExporterOutput("ReporteEmployeePdf.pdf"));
        SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
        exp.setConfiguration(conf);
        exp.exportReport();

        // se muestra en una ventana aparte para su descarga
        JasperPrint jasperPrintWindow = JasperFillManager.fillReport("C:\\Java\\Workspace\\ProjectOracleJasper\\listEmployeeConexion.jasper", parametros, Conexion.conectar());
        JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow);
        jasperViewer.setVisible(true);
    }
}
