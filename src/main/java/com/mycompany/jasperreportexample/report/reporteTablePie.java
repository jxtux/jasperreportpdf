package com.mycompany.jasperreportexample.report;

import com.mycompany.jasperreportexample.conexion.Conexion;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class reporteTablePie {

    public static void main(String[] args) throws JRException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        File miDir = new File(".");
        String separador = System.getProperty("file.separator");
        String ruta = miDir.getCanonicalPath() + separador + "report" + separador;

        //PARAMETROS
        Map parametros = new HashMap();
        parametros.put("departamentId", 100);

        // descarga dentro del mismo proyecto
        JasperPrint jasperPrint = JasperFillManager.fillReport(ruta+"Table_PieChart.jasper", parametros, Conexion.conectar());
        JRPdfExporter exp = new JRPdfExporter();
        exp.setExporterInput(new SimpleExporterInput(jasperPrint));
        exp.setExporterOutput(new SimpleOutputStreamExporterOutput("ReportePiePdf.pdf"));
        SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
        exp.setConfiguration(conf);
        exp.exportReport();

        // se muestra en una ventana aparte para su descarga
        JasperPrint jasperPrintWindow = JasperFillManager.fillReport(ruta+"Table_PieChart.jasper", parametros, Conexion.conectar());
        JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow);
        jasperViewer.setVisible(true);
    }
}
